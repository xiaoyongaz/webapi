'use strict';
const connectionStrings = 
    ['HostName=TijeeIOT.azure-devices.net;SharedAccessKeyName=registryReadWrite;SharedAccessKey=D/CkgXmXtXpQwiulVgviJm3vpn+iuWUQEVTRgI6aFgY=',
     'HostName=TijeeIOT.azure-devices.net;SharedAccessKeyName=registryReadWrite;SharedAccessKey=aqji2vVZaIXFcEghzzaWheHC4qZX+HPTzoOrrj59UQc='];
const IOTSERVICEPORT = 3004;

var iothub = require('azure-iothub');
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var bodySerializer = {
    post: jsonParser,
    patch: jsonParser
}


var device = new iothub.Device(null);

var DevicesHandler = {
    get: function getAzureIOTDevice(req, res) {
       var iotRegistry = iothub.Registry.fromConnectionString(connectionStrings[0]);
        var deviceId = req.params.deviceId;
        iotRegistry.get(deviceId, function getIOTDeviceHandler(err, deviceInfo, innerres) {
            if(err){
                req.log.info(err);
            }
           if(innerres)
            {
                req.log.info(`response code is ${innerres.statusCode}, response msg is ${innerres.statusMessage}`);
            }
            if(deviceInfo)
            {
                res.send(deviceInfo);
            }
            else
            {
                res.end();    
            }            
        })                
    },
    create: function createAzureIOTDevice(req, res) {
        //{deviceId:{deviceId}, status: 'initializing', statusReason: 'Binding device with IOTHub'}
        var device = new iothub.Device(null);
        var iotRegistry = iothub.Registry.fromConnectionString(connectionStrings[0]);
        device.deviceId = req.params.deviceId;
        device.status = req.body.status;
        device.statusReason = req.body.statusReason;
        iotRegistry.create(device, function createIOTDeviceHandler(err, deviceInfo, innerres){
            if(err){
                req.log.info(err);
            }
            if(innerres)
            {
                req.log.info(`response code is ${innerres.statusCode}, response msg is ${innerres.statusMessage}`);
            }
            if(deviceInfo)
            {
                res.send(deviceInfo);
            }
            else
            {
                res.end();
            }
        })
    },
 
    delete: function delAzureIOTDevice(req, res) {
        var iotRegistry = iothub.Registry.fromConnectionString(connectionStrings[0]);
        var deviceId = req.params.deviceId;
        iotRegistry.delete(deviceId, function deleteIOTDeviceHandler(err, deviceInfo, innerres) {
            if(err){
                req.log.info(err);
            }
           if(innerres)
            {
                req.log.info(`response code is ${innerres.statusCode}, response msg is ${innerres.statusMessage}`);
            }
            if(deviceInfo)
            {
                res.send(deviceInfo);
            }
            else
            {
                res.end();    
            }            
        })        
    }
}     

app.use(require('express-bunyan-logger')());
app.map = function(a, route){
  route = route || '';
  for (var key in a) {
    switch (typeof a[key]) {
      // { '/path': { ... }}
      case 'object':
        app.map(a[key], route + key);
        break;
      // get: function(){ ... }
      case 'function':
        console.log('%s %s', key, route);
        if(bodySerializer[key])
        {
            app[key](route, bodySerializer[key], a[key]);
        }
        else
        {
            app[key](route, a[key]);
        }
        break;
    }
  }
};

app.map({
  '/IOTDevices/:deviceId': {
      get: DevicesHandler.get,
      post: DevicesHandler.create,
      delete: DevicesHandler.delete
    }
});

// middleware with an arity of 4 are considered
// error handling middleware. When you next(err)
// it will be passed through the defined middleware
// in order, but ONLY those with an arity of 4, ignoring
// regular middleware.
app.use(function(err, req, res, next){
  // whatever you want here, feel free to populate
  // properties on `err` to treat it differently in here.
  res.status(err.status || 500);
  res.send({ error: err.message });
});

// our custom JSON 404 middleware. Since it's placed last
// it will be the last middleware called, if all others
// invoke next() and do not respond.
app.use(function(req, res, next){
  res.status(404);
  res.send({ error: `Operation ${req.path} is not support` });
});

app.listen(IOTSERVICEPORT);
console.log('Tijee IOT registry service started on port 3004');
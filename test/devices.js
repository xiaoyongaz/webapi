const auth0ClientID = "sEwOLDFwrA1ynbcGmFViJiCopmGfTKtI"
const auth0ClientSec = "qSEedBc8cNFw8NJiM5h2kSnLLwOtqs_1a6Rv8VPt0_v5WqV8390sbgZx6FXxhmBb"
const auth0ClientSecEncode = new Buffer(auth0ClientSec, 'base64');

const deviceApiScopes = {
  create : {
    "users": {
      "actions": [
        "create"
      ]
    }
  },
  get : {
    "users": {
      "actions": [
        "read"
      ]
    }
  },
  delete : {
    "users": {
      "actions": [
        "delete"
      ]
    }
  },
  update : {
    "users": {
      "actions": [
        "update"
      ]
    }
  }, 
  all :{
    "users": {
      "actions": [
        "create",
        "read",
        "delete",
        "update"
      ]
    }      
  }
};
const jwtOptions = {
    expiresIn : "1h",
    audience: auth0ClientID,
    jwtid: "97ec23fadd95f3862ae423da66a64f0c"
};

var should = require('chai').should();
var request = require('supertest');
var jwt = require('jsonwebtoken');
var token = jwt.sign({scopes:deviceApiScopes.all}, auth0ClientSecEncode, jwtOptions);
var uuid = require('node-uuid')

describe('IOT device directory service', function(){
    //var url = 'http://localhost:3003'
    var url = 'http://devicedirectory.dockercloud-quickstart-python.efd2180c.svc.dockerapp.io:3003'
  it('get all devices', function(done){
   request(url)
    .get('/Devices')
    .set('Authorization', `Bearer ${token}` )
    .expect(200, done);
  })

  it('create a device', function(done){
    this.timeout(0);
    var deviceId = uuid.v1();
    const a0user = 'auth0|56f18132246eddef35685f5e';
    request(url)
    .post(`/Devices/${deviceId}`)    
    .set('Authorization', `Bearer ${token}` )
    .send(  { a0id: a0user, id:deviceId, State: "created", lastUpdate: "2016-03-22", ConnectionState:"not connected", ConnectionUpdateTime:"2016-03-22" })  
    .end(function(err,res){
        res.body.cloudToDeviceMessageCount.should.be.eql(0);
        res.body.connectionState.should.be.eql("Disconnected");
        res.body.deviceId.should.be.eql(deviceId);
        res.body.status.should.be.eql("enabled");
        res.body.statusReason.should.be.eql("Binding device with IOTHub");
        res.body.a0Id.should.be.eql(a0user);
        done();
    });
  })
  
  describe('Get a device',  function(done){
    var deviceId = uuid.v1();
    var testDevice = { a0id: "TijeeUser1", id:deviceId, State: "created", lastUpdate: "2016-03-22", ConnectionState:"not connected", ConnectionUpdateTime:"2016-03-22" };
    before(function(done){
        request(url)
        .post(`/Devices/${deviceId}`)    
        .set('Authorization', `Bearer ${token}` )
        .send(testDevice)  
        .end(function(err,res){
            res.body.cloudToDeviceMessageCount.should.be.eql(0);
            res.body.connectionState.should.be.eql("Disconnected");
            res.body.deviceId.should.be.eql(deviceId);
            res.body.status.should.be.eql("enabled");
            res.body.statusReason.should.be.eql("Binding device with IOTHub");
            res.body.a0Id.should.be.eql("TijeeUser1");
            done();
         })
      });
      
     it('get precreated device', function(done){
       request(url)
        .get(`/Devices/${deviceId}`)
        .set('Authorization', `Bearer ${token}` )
        .end(function(er,res){
            res.body.cloudToDeviceMessageCount.should.be.eql('0');
            res.body.connectionState.should.be.eql("Disconnected");
            res.body.deviceId.should.be.eql(deviceId);
            res.body.status.should.be.eql("enabled");
            res.body.statusReason.should.be.eql("Binding device with IOTHub");
            res.body.a0Id.should.be.eql("TijeeUser1");
            done();     
        })   
     });     
    });
    
  describe('Delete a device',  function(done){
    var deviceId = uuid.v1();
    var testDevice = { a0id: "TijeeUser1", id:deviceId, State: "created", lastUpdate: "2016-03-22", ConnectionState:"not connected", ConnectionUpdateTime:"2016-03-22" };
    before(function(done){
        request(url)
        .post(`/Devices/${deviceId}`)    
        .set('Authorization', `Bearer ${token}` )
        .send(testDevice)  
        .end(function(err,res){
            res.body.cloudToDeviceMessageCount.should.be.eql(0);
            res.body.connectionState.should.be.eql("Disconnected");
            res.body.deviceId.should.be.eql(deviceId);
            res.body.status.should.be.eql("enabled");
            res.body.statusReason.should.be.eql("Binding device with IOTHub");
            res.body.a0Id.should.be.eql("TijeeUser1");
            done();
         })
      });
      
     it('delete precreated device', function(done){
       request(url)
        .delete(`/Devices/${deviceId}`)
        .set('Authorization', `Bearer ${token}` )
        .expect(200,done)
     });     
    });    
});

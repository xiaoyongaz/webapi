var should = require('chai').should();
var request = require('supertest');
var uuid = require('node-uuid');
var createDeviceTemplate = {status: 'enabled', statusReason:'Binding device with IOTHub'}
const url = 'http://tijeeiothub-1.6004be41.cont.dockerapp.io:3004';
describe('IOTHub device directory tests', function(){
    //var url = 'http://localhost:3004'
    var deviceId
    
    it('create a device registration', function(done){
        this.timeout(0);
        deviceId = uuid.v1();
        createDeviceTemplate.deviceId = deviceId        
        request(url)
        .post(`/IOTDevices/${deviceId}`)
        .send(createDeviceTemplate)
        .end(function(err, res){
            res.body.deviceId.should.be.equal(deviceId, "created deviceId should be same as input")
            done(err)
        })
    });
    
    describe('get or delete device', function(done){
        before(function(done){
            deviceId = uuid.v1();
            createDeviceTemplate.deviceId = deviceId
            request(url)
            .post(`/IOTDevices/${deviceId}`)
            .send(createDeviceTemplate)
            .end(function(err, res){
                done(err)
            })
        });
        
        /*
        after(function(done){
            request(url)
            .delete(`/IOTDevices/${deviceId}`)
            .end(function(err, res){
                done(err)
            })
        });
        */
        
        it('get a device registration', function(done){
            request(url)
                .get(`/IOTDevices/${deviceId}`)
                .end(function(er,res){
                    res.body.deviceId.should.be.eql(deviceId);
                    done();     
                })           
        });
        
        it('delete a device registration', function(done){
            request(url)
            .delete(`/IOTDevices/${deviceId}`)
            .end(function(err, res){
                done(err)
            })
        })
        
    })
})

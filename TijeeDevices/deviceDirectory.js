
"use strict";
//iot hub SASs can be excluded in the directory, 
//it can be queried from IOT registry api 

/*DeviceDetail schema
{
    "authentication": {
        "SymmetricKey":{
            "primaryKey":"key1",
            "secondaryKey":"key2"
        }
    },
    "cloudToDeviceMessageCount": 0,
    "connectionState":"Disconnected",
    "connectionStateUpdateTime":"",
    "deviceId":"",
    "generationId":"",
    "lastActivityTime":"",
    "status":"",
    "statusReason":"",
    "statusUpdatedTime":""
}
*/

function SymmetricKey(primaryKey, secondaryKey){
    this.primaryKey = primaryKey;
    this.secondaryKey = secondaryKey;    
}

function authentication(symmetricKey){
    this.SymmetricKey = symmetricKey;
}

function DeviceDetail(resBody, auth0UserId){
    //todo:first validate resBody
    this.authentication = new authentication(new SymmetricKey(resBody.authentication.SymmetricKey.primaryKey, resBody.authentication.SymmetricKey.secondaryKey));
    this.cloudToDeviceMessageCount = resBody.cloudToDeviceMessageCount;
    this.connectionState = resBody.connectionState;
    this.connectionStateUpdatedTime = resBody.connectionStateUpdatedTime;
    this.deviceId = resBody.deviceId;
    this.generationId = resBody.generationId;
    this.lastActivityTime = resBody.lastActivityTime;
    this.status = resBody.status;
    this.statusReason = resBody.statusReason;
    this.statusUpdatedTime = resBody.statusUpdatedTime;
    this.a0Id = auth0UserId;
}

var createDeviceTemplate = {status: 'enabled', statusReason:'Binding device with IOTHub'}

// types in redis: 
// 1. hash with key deviceId:{devicdId}
// 2. a set with name devicekeys, that contains all deviceIds 
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();
var bodySerializer = {
    post: jsonParser,
    patch: jsonParser
}
var request = require('superagent');
var redis = require('redis')
var redisClient = redis.createClient({host:"tijeeredis-1.c36f3871.cont.dockerapp.io"});

const DEVICEKEYNAME = "AllDevices"
const DEVICEIDPREFIX = "id:"
const auth0ClientID = "sEwOLDFwrA1ynbcGmFViJiCopmGfTKtI"
const auth0ClientSec = "qSEedBc8cNFw8NJiM5h2kSnLLwOtqs_1a6Rv8VPt0_v5WqV8390sbgZx6FXxhmBb"
const auth0ClientSecEncode = new Buffer(auth0ClientSec, 'base64');
const IOTHubServiceUrl = 'http://tijeeiothub-1.6004be41.cont.dockerapp.io:3004'

//autho account tjc, application tjcam
const auth0ManagementApiKey = '3AFcQ5O76C16GtvdyeRGtWgfV1B57Q8V'
const auth0ManagementApiSecret = '6IkB1wkGQQJ3VouNLehA5KLBHhL3hcBn0UtNYRBScm1OFCPROhCHgE2JVn9hQsSO'
const auth0SecretEncoded = new Buffer(auth0ManagementApiSecret, 'base64');

function getIAT(){
    return date.getTime()/1000;    
}

function getEXP(delayInSeconds){
    return (date.getTime() + delayInSeconds*1000)/1000;
}

const userApiScopes = {
  create : {
    "users": {
      "actions": [
        "create"
      ]
    }
  },
  get : {
    "users": {
      "actions": [
        "read"
      ]
    }
  },
  delete : {
    "users": {
      "actions": [
        "delete"
      ]
    }
  },
  update : {
    "users": {
      "actions": [
        "update"
      ]
    }
  }, 
  all :{
    "users": {
      "actions": [
        "create",
        "read",
        "delete",
        "update"
      ]
    }      
  }
};

const jwtOptions = {
    expiresIn : "1h",
    audience: auth0ManagementApiKey,
    jwtid: "97ec23fadd95f3862ae423da66a64f0c"
};

var jsonwebtoken = require('jsonwebtoken');
var allOpsToken = jsonwebtoken.sign({scopes:userApiScopes.all}, auth0SecretEncoded, jwtOptions);
var Auth0ManagementClient = require('auth0').ManagementClient;
var auth0Management = new Auth0ManagementClient({
   domain: 'tjc.auth0.com',
   token: /*createUserToken*/ allOpsToken
});

var DevicesHandler = {
    list: function(req, res){
        //todo: this is for prototype only, real one should only return a limited number
        req.log.info("list all devices")
        redisClient.SMEMBERS(DEVICEKEYNAME, function (err, deviceKeys){
            if(err) throw (err);
            var device;
            var count = 0;
            if(deviceKeys.length == 0) 
            {
                res.end();
            }
            else
            {
                deviceKeys.forEach(function(entry){
                    redisClient.HGETALL(entry, function(err, deviceInfo){
                        if(err) throw (err);
                        res.write(JSON.stringify(deviceInfo));
                        count++;
                        if(count == deviceKeys.length)
                        {
                            res.end();
                        }
                    });            
                });
            }     
         });
    },
    
    get: function(req, res){      
        var device;
        req.log.info(`trying to get device id ${DEVICEIDPREFIX}${req.params.deviceId}`)
        redisClient.HGETALL(`${DEVICEIDPREFIX}${req.params.deviceId}`, function(err, deviceInfo){
            if(err) 
            {
                req.log.info('getting device err' + err);
                throw(err);
            }
            
            if(deviceInfo)
            {
                req.log.info('get device');
                req.log.info(deviceInfo);
                res.send(deviceInfo);
            }
        });
    },
    
    create: function(req, res){
        if(!req.body) return res.sendStatus(400);
        req.log.info('create a new device');
        req.log.info(`user info is : ${req.user}`);
        req.log.info('need to validate body')
        //todo: need to make two operation a transactional
        //todo: validate input, need a0id
        createDeviceTemplate.deviceId = req.params.deviceId;
        request.post(`${IOTHubServiceUrl}/IOTDevices/${req.params.deviceId}`)
            .send(createDeviceTemplate)
            .end(function(err, iothubres){
                if(err)
                {
                    throw (err);
                }
                else
                {
                    let deviceDetail = new DeviceDetail(iothubres.body, req.body.a0id);
                    redisClient.HMSET(`${DEVICEIDPREFIX}${req.params.deviceId}`, deviceDetail);
                    redisClient.SADD(DEVICEKEYNAME, `${DEVICEIDPREFIX}${req.params.deviceId}`); 
                    auth0Management.updateAppMetadata(
                        {id:req.body.a0id},
                        {devices:
                            { iotDeviceKeys : 
                                {
                                    primary:deviceDetail.authentication.SymmetricKey.primaryKey,
                                    secondary:deviceDetail.authentication.SymmetricKey.secondaryKey
                                }
                            }
                        }, function(err, user){
                        
                            res.send(deviceDetail);
                    });      
                    
                }
            })
    },
    
    update: function(req, res){
        if(!req.body) return res.sendStatus(400);
        req.log.info('create a new device')
        redisClient.HMSET(`${DEVICEIDPREFIX}${req.params.deviceId}`, req.body);       
        res.send(req.body)
    },
    
    delete: function(req, res){
        req.log.info("delete a device:" + req.params.deviceId);
        //todo: do it in a transaction
        redisClient.DEL(`${DEVICEIDPREFIX}${req.params.deviceId}`);
        redisClient.SREM(DEVICEKEYNAME, req.params.deviceId);
        res.end();
    }
}

app.use(require('express-bunyan-logger')());
var jwt = require('express-jwt');
app.use(jwt({secret: auth0ClientSecEncode}));

app.map = function(a, route){
  route = route || '';
  for (var key in a) {
    switch (typeof a[key]) {
      // { '/path': { ... }}
      case 'object':
        app.map(a[key], route + key);
        break;
      // get: function(){ ... }
      case 'function':
        console.log('%s %s', key, route);
        if(bodySerializer[key])
        {
            app[key](route, bodySerializer[key], a[key]);
        }
        else
        {
            app[key](route, a[key]);
        }
        break;
    }
  }
};

app.map({
  '/Devices': {
    get: DevicesHandler.list,
    '/:deviceId': {
      get: DevicesHandler.get,
      post: DevicesHandler.create,
      patch: DevicesHandler.update,
      delete: DevicesHandler.delete
    }
  }
});

// middleware with an arity of 4 are considered
// error handling middleware. When you next(err)
// it will be passed through the defined middleware
// in order, but ONLY those with an arity of 4, ignoring
// regular middleware.
app.use(function(err, req, res, next){
  // whatever you want here, feel free to populate
  // properties on `err` to treat it differently in here.
  res.status(err.status || 500);
  res.send({ error: err.message });
});

// our custom JSON 404 middleware. Since it's placed last
// it will be the last middleware called, if all others
// invoke next() and do not respond.
app.use(function(req, res, next){
  res.status(404);
  res.send({ error: `Operation ${req.path} is not support` });
});

app.listen(3003);
console.log('Tijee directory service started on port 3003');

//auth0 token for account tjc, application tjcam and scope users:create 
//eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiIzQUZjUTVPNzZDMTZHdHZkeWVSR3RXZ2ZWMUI1N1E4ViIsInNjb3BlcyI6eyJ1c2VycyI6eyJhY3Rpb25zIjpbImNyZWF0ZSJdfX0sImlhdCI6MTQ1OTE5ODYxMSwianRpIjoiN2U1NDY3ZGQ1MDVhZGQ0MDQwZTIyYjcxZTAzNTQ4NDQifQ.-Z2iSdPh-jPcfT5_kl1FZ06pHgSUg2Dz4mfoFQqj03o

//autho account tjc, application tjcam
const auth0ManagementApiKey = '3AFcQ5O76C16GtvdyeRGtWgfV1B57Q8V'
const auth0ManagementApiSecret = '6IkB1wkGQQJ3VouNLehA5KLBHhL3hcBn0UtNYRBScm1OFCPROhCHgE2JVn9hQsSO'
const auth0SecretEncoded = new Buffer(auth0ManagementApiSecret, 'base64');

function getIAT(){
    return date.getTime()/1000;    
}

function getEXP(delayInSeconds){
    return (date.getTime() + delayInSeconds*1000)/1000;
}

const userApiScopes = {
  create : {
    "users": {
      "actions": [
        "create"
      ]
    }
  },
  get : {
    "users": {
      "actions": [
        "read"
      ]
    }
  },
  delete : {
    "users": {
      "actions": [
        "delete"
      ]
    }
  },
  update : {
    "users": {
      "actions": [
        "update"
      ]
    }
  }, 
  all :{
    "users": {
      "actions": [
        "create",
        "read",
        "delete",
        "update"
      ]
    }      
  }
};

const jwtOptions = {
    expiresIn : "1h",
    audience: auth0ManagementApiKey,
    jwtid: "97ec23fadd95f3862ae423da66a64f0c"
};

var jwt = require('jsonwebtoken');

var createUserToken = jwt.sign({scopes:userApiScopes.create}, auth0SecretEncoded, jwtOptions);
var allOpsToken = jwt.sign({scopes:userApiScopes.all}, auth0SecretEncoded, jwtOptions);
var ManagementClient = require('auth0').ManagementClient;

var management = new ManagementClient({
   domain: 'tjc.auth0.com',
   token: /*createUserToken*/ allOpsToken
});

var CONNECTION = 'Username-Password-Authentication';

var userData = {
    "email": "testuser11@tijee.com",
    "password":"secret",
    "email_verified": false,
    "connection": CONNECTION
};

var createdUserData;

management.createUser(userData, function(err, user){
    if(err)
    {
        console.log('Error creating user:' + err);
    }
    else
    {
        console.log('created user')
        createdUserData = user;
        management.updateUser({id:createdUserData.user_id}, {"email_verified": true}, function (err, user){
            if(err)
            {
                console.log('error updating user:' + err);
            }
            else
            {
                console.log('updated user')
                createdUserData = user;
                management.updateUserMetadata({id:createdUserData.user_id}, {address: 'issaqua, wa, 98888'}, function(err, user){
                  if(err)
                  {
                      console.log("error updating user meta data");
                  }
                  else
                  {
                    management.updateAppMetadata({id:createdUserData.user_id}, {devices: {"device1": "deviceSASKey1", "devices2": "deviceSASKey2"}}, function(err, user){
                        if(err)
                        {
                            console.log('error updating app meta data');
                        }
                        else
                        {
                            management.getUser({id:createdUserData.user_id}, function(err, user){
                                if(err)
                                {
                                    console.log('error getting user:' + err);
                                }
                                else
                                {
                                    console.log('got user')
                                    createdUserData = user;
                                    management.deleteUser({id:createdUserData.user_id}, function(err){
                                        if(err)
                                        {
                                            console.log('error deleting user:' + err);
                                        }
                                        else
                                        {
                                            console.log("user deleted");
                                        }
                                    });
                                }
                            });
                        }
                     })        
                   }  
                })
            }
        });
    }
});



